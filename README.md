# 13Pixels Validation
A simple framework for validating prefabs (perhaps, eventually also scenes)
by giving components a validation method that is run by a test case.

Instead of loosely defining how to use a component in a wiki, define constraints
in the component itself and let the Test Runner find out whether your project is fulfilling them.

# How to use
1. Import the package (you can use the package manager, remember to [enable tests](https://docs.unity3d.com/Manual/cus-tests.html#tests)).
2. Add the `IValidatable` interface to your component, like this:
    ```csharp
    using UnityEngine;
    using System.Collections.Generic;
    using ThirteenPixels.Validation;
    
    [RequireComponent(typeof(Collider))]
    public class DeathTrigger : MonoBehaviour, IValidatable
    {
        private void OnTriggerEnter(Collider other)
        {
            // ...
        }
    
        void IEnumerable<ValidationResult> IValidatable.Validate()
        {
            var collider = GetComponent<Collider>();
    
            if (!collider) yield return "No Collider was found."; // Check this just in case a prefab was made before [RequireComponent] was added
            else if (!collider.isTrigger) yield return "The collider is not set to isTrigger.";
        }
    }
    ```
    Yield-return any number of strings, one for each detected issue.
3. [Use the Test Runner](https://docs.unity3d.com/Packages/com.unity.test-framework@1.1/manual/getting-started.html) to have every prefab in your project checked.
4. If one or more prefabs fail validation, a window will open that displays a list of all issues:
    
    ![Validator Window](https://i.imgur.com/Aj6InOK.png)
    
    You can go through these issues by clicking the button on the left, fixing it, and removing the issue from the list with the "Done" button.
    Remember to run the test again once the list is empty!
