﻿
namespace ThirteenPixels.Validation.Test
{
    using NUnit.Framework;
    using ThirteenPixels.Validation.Editor;

    public class ValidationTest
    {
        [Test]
        public void ValidateAllPrefabs()
        {
            ValidatorCore.ValidateAllPrefabs();

            if (ValidatorCore.hasFailedValidations)
            {
                Assert.Fail("One or more prefabs in your project failed validation. Open the Validation Window for details.");
            }
        }
    }
}
