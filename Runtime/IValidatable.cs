﻿
namespace ThirteenPixels.Validation
{
    using System.Collections.Generic;

    public interface IValidatable
    {
        /// <summary>
        /// Runs validation for this component instance.
        /// Yield return a string for each issue, describing what's wrong.
        /// </summary>
        IEnumerable<ValidationIssue> Validate();
    }
}
