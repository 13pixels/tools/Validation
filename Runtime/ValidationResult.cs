﻿
namespace ThirteenPixels.Validation
{
    using System;

    /// <summary>
    /// A single issue detected by validation, like a missing component.
    /// </summary>
    public struct ValidationIssue
    {
        public readonly string failureMessage;

        public ValidationIssue(string failureMessage)
        {
            this.failureMessage = failureMessage;
        }

        public static implicit operator ValidationIssue(string message)
        {
            return new ValidationIssue(message);
        }
    }
}
