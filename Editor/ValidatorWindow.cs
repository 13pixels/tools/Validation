﻿
namespace ThirteenPixels.Validation.Editor
{
    using UnityEngine;
    using UnityEditor;
    
    internal class ValidatorWindow : EditorWindow
    {
        private Vector2 scrollPosition;

        internal static void Open()
        {
            EditorApplication.delayCall += () => GetWindow<ValidatorWindow>("Validation").Show();
        }

        private void OnGUI()
        {
            GUILayout.Label("", EditorStyles.toolbar, GUILayout.ExpandWidth(true));
            GUI.Label(new Rect(4, -2, 150, 24), "Validation failures", EditorStyles.boldLabel);
            if (GUI.Button(new Rect(position.width - 120, 1, 116, 24), "Open Test Runner", EditorStyles.miniButton))
            {
                GetWindow<UnityEditor.TestTools.TestRunner.TestRunnerWindow>();
            }

            if (ValidatorCore.hasFailedValidations)
            {
                scrollPosition = GUILayout.BeginScrollView(scrollPosition, false, true);

                DrawScrollViewContent();

                GUILayout.EndScrollView();
            }
            else
            {
                EditorGUILayout.HelpBox("No known failed validations.", MessageType.Info);
            }
        }

        private static void DrawScrollViewContent()
        {
            for (var i = 0; i < ValidatorCore.failedValidations.Count; i++)
            {
                var result = ValidatorCore.failedValidations[i];

                GUILayout.BeginHorizontal(EditorStyles.helpBox);

                DrawPrefabButton(result.sourceObject);

                GUILayout.BeginVertical();
                DrawHeader(ref i, result);
                DrawIssuesOrException(result);
                GUILayout.EndVertical();

                GUILayout.EndHorizontal();
            }
        }

        private static void DrawHeader(ref int i, ValidationResult result)
        {
            GUILayout.BeginHorizontal();
            DrawComponentTypeLabel(result.componentType);
            if (DrawDoneButton())
            {
                ValidatorCore.failedValidations.RemoveAt(i);
                i--;
            }
            GUILayout.EndHorizontal();
        }

        private static void DrawPrefabButton(GameObject sourceObject)
        {
            if (GUILayout.Button(sourceObject.name, GUILayout.Width(120)))
            {
                Selection.activeObject = sourceObject;
                EditorGUIUtility.PingObject(sourceObject.transform.root.gameObject);
            }
        }

        private static void DrawComponentTypeLabel(System.Type type)
        {
            GUILayout.Label(type.Name, EditorStyles.boldLabel, GUILayout.ExpandWidth(true), GUILayout.Height(18));
        }

        private static bool DrawDoneButton()
        {
            var previousColor = GUI.color;
            GUI.color *= new Color(0.5f, 1f, 0.5f);
            var clicked = GUILayout.Button("Done", GUILayout.Width(80));
            GUI.color = previousColor;
            return clicked;
        }

        private static void DrawIssuesOrException(ValidationResult result)
        {
            if (result.exception == null)
            {
                foreach (var issue in result.issues)
                {
                    GUILayout.Label("• " + issue.failureMessage);
                }
            }
            else
            {
                EditorGUILayout.HelpBox("Exception during validation:\n" + result.exception.ToString(), MessageType.Error);
            }
        }
    }
}
