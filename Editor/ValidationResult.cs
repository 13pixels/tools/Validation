﻿
namespace ThirteenPixels.Validation
{
    using UnityEngine;
    using System.Collections.Generic;

    /// <summary>
    /// The result of a failed validation.
    /// Lists the issues found on a specified object.
    /// </summary>
    public struct ValidationResult
    {
        public readonly GameObject sourceObject;
        public readonly System.Type componentType;
        public readonly IEnumerable<ValidationIssue> issues;
        public readonly System.Exception exception;

        public ValidationResult(GameObject sourceObject, System.Type componentType, IEnumerable<ValidationIssue> issues)
        {
            this.sourceObject = sourceObject;
            this.componentType = componentType;
            this.issues = issues;
            exception = null;
        }

        public ValidationResult(GameObject sourceObject, System.Type componentType, System.Exception exception)
        {
            this.sourceObject = sourceObject;
            this.componentType = componentType;
            this.exception = exception;
            issues = null;
        }
    }
}
