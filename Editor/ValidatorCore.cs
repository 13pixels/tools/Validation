﻿#define INSTANTIATE_PREFAB

namespace ThirteenPixels.Validation.Editor
{
    using UnityEngine;
    using UnityEditor;
    using System.Collections.Generic;
    using System.Linq;


    public static class ValidatorCore
    {
        internal static readonly List<ValidationResult> failedValidations = new List<ValidationResult>();
        public static bool hasFailedValidations => failedValidations.Count > 0;

        public static void ValidateAllPrefabs(bool openWindowAfterFailing = true)
        {
            failedValidations.Clear();

            foreach (var prefab in FindAllPrefabsInProject())
            {
#if INSTANTIATE_PREFAB
                var instance = Object.Instantiate(prefab);
#else
                var instance = prefab;
#endif
                var validatables = instance.GetComponentsInChildren<IValidatable>();

                foreach (var validatable in validatables)
                {
#if INSTANTIATE_PREFAB
                    var validatedGameObject = prefab;
#else
                    var validatedGameObject = ((Component)validatable).gameObject;
#endif

                    try
                    {
                        List<ValidationIssue> issues = null;
                        foreach (var issue in validatable.Validate())
                        {
                            if (issues == null)
                            {
                                issues = new List<ValidationIssue>();
                            }
                            issues.Add(issue);
                        }

                        if (issues != null)
                        {
                            failedValidations.Add(new ValidationResult(validatedGameObject, validatable.GetType(), issues));
                        }
                    }
                    catch (System.Exception e)
                    {
                        failedValidations.Add(new ValidationResult(validatedGameObject, validatable.GetType(), e));
                    }
                }

#if INSTANTIATE_PREFAB
                Object.DestroyImmediate(instance);
#endif
            }

            if (openWindowAfterFailing && hasFailedValidations)
            {
                ValidatorWindow.Open();
            }
        }

        private static IEnumerable<GameObject> FindAllPrefabsInProject()
        {
            return AssetDatabase.FindAssets("t:prefab").Select(path => AssetDatabase.LoadAssetAtPath<GameObject>(AssetDatabase.GUIDToAssetPath(path)));
        }
    }
}
